#!/usr/bin/env python3

import numpy as np
from numpy.random import rand
import matplotlib.pyplot as plt

class Ising():
       
    def initialstate(N):
        ''' Generates a random spin configuration for inital condition'''
        state = 2 * np.random.randint(2,size=(N,N)) - 1
        return state

    def monte(config,N,beta):
        ''' Monte Carlo moves using Metropolis algorithm'''
        for i in range(N):
            for j in range(N):
                   a = np.random.randint(0,N) # returns random number, lattice site coordinate
                   b = np.random.randint(0,N) # returns random number, lattice site coordinate
                   spin = config[a,b]         # spin configuration
                   neighbours = config[(a +1)%N,b] + config[a,(b+1)%N] + config[(a-1)%N,b] + config[a,(b-1)%N] + config[(a+1)%N,(b+1)%N] + config[(a-1)%N,(b-1)%N]                       #interaction with its neighbours spin
                   dE = 2 * spin * neighbours # energy difference for a spin flip
                   if dE < 0:                 # probabiltiy of choosing this spin state      
                       spin *= -1
                   elif rand() < np.exp(-dE * beta):
                       spin *= -1
                   config[a,b] = spin
        return config


    def energies(config):
        ''' Energy of a given configuration'''
        energy = 0
        for i in range(len(config)):
            for j in range(len(config)):
                spin = config[i,j]                   # spin configuration
                neighbour = config[(i + 1)%N, j] + config[i, (j + 1)%N] + config[(i - 1)%N, j] + config[i, (j - 1)%N] + config[(i + 1)%N,(j + 1)%N] + config[(i - 1)%N,(j - 1)%N]                  # interaction with its neighbours spin two dimensions
                energy += -neighbour * spin 
        return energy/6



    def magnetic(config):
        ''' Magnetization of a given configuration'''
        magnet = np.sum(config)          # magnetization of a given configuration in the lattice.
        return magnet


    def movie(self):
        ''' This function simulates the ising model'''
        N = 64      # size of lattice
        temp = 3.7    # temperature of lattice,        
        config = 2 * np.random.randint(2,size=(N,N)) - 1  # defining inital state
        c  = plt.figure(figsize=(15, 15), dpi=80)
        self.domainPlot(c,config,0,N,1)  # plotting inital state of ups and downs

        ms = 1001   # number of iterations
        for i in range(ms):
            Ising.monte(config,N,1.0/temp)   # using Monte function to see change
            if i == 1:        self.domainPlot(c, config, i, N, 2)
            if i == 2:        self.domainPlot(c, config, i, N, 3)
            if i == 3:        self.domainPlot(c, config, i, N, 4)
            if i == 4:        self.domainPlot(c, config, i, N, 5)
            if i == 5:        self.domainPlot(c, config, i, N, 6)
            if i == 150:      self.domainPlot(c, config, i, N, 7)
            if i == 500:      self.domainPlot(c, config, i, N, 8)
            if i == 1000:     self.domainPlot(c, config, i, N, 9)
                 
    def domainPlot(self,c, config, i, N, n_):
        '''This function plots the configuration once passed to it along with time'''
        X, Y = np.meshgrid(range(N), range(N))
        #subplot =  c.add_subplot(3, 3, n_ )  
        #plt.setp(subplot.get_yticklabels(), visible = False)
        #plt.setp(subplot.get_xticklabels(), visible = False)      
        plt.pcolormesh(X, Y, config, cmap = plt.cm.RdBu)
        plt.title('Temperature = 3.7(J/Kb) & Time=%d'%i) 
        plt.axis('tight')    
        plt.show()                    
    

rm = Ising()
rm.movie() # run the movie to see the domains 


################################parameters that can be changed below if you want to simulate a smaller system.#########################################

tpoint   = 2**8                       # Temperature points that will be in graph
N        = 2**4                       # Size of latice 16 x 16
eq_steps = 2**10                      # equlibrium steps needed
mc_steps = 2**10                      # Monte Carlo steps needed

n1 = 1.0/(mc_steps * N * N)           # used for increments 
n2 = 1.0/(mc_steps *mc_steps * N * N)
tc = 3.7                              # used as critial temp
t  = np.random.normal(tc, .64,tpoint) # random distrubition of temp
t  = t[(t > 2.0) & (t < 4.5)]         # range 
tpoint = np.size(t)                   # size of temp

Energy = np.zeros(tpoint)             # zero array of Energy
Magnetization = np.zeros(tpoint)      # zero array of mag
Specificheat = np.zeros(tpoint)       # zero array of specf
Susceptibility = np.zeros(tpoint)     # zero array of susc

#######################################################################################################################################################
# Main code ###

for x in range(len(t)):
    E1 = M1 = E2 = M2 = 0            # Initialise
    
    config = Ising.initialstate(N)
    it = 1.0/t[x]                    # beta for Specific heat
    it2 = it * it                    # beta for suscpectibility
    
    for i in range(eq_steps):        # equilibrium steps
        Ising.monte(config, N, it)
   
    for i in range(mc_steps):        # monte carlo steps, updating system
        Ising.monte(config, N, it)
        calene = Ising.energies(config)
        calmag = Ising.magnetic(config)
     
        E1 = E1 + calene
        E2 = E2 + calene * calene
        M1 = M1 + calmag
        M2 = M2 + calmag * calmag
        
        Energy[x]         = n1 * E1
        Magnetization[x]  = n1 * M1
        Specificheat[x]   = (n1 * E2 - n2 * E1 * E1) * it2
        Susceptibility[x] = (n1 * M2 - n2 * M1 * M1) * it


print(Energy*6) # calculate the ground state 



############################################### Plots ############################################################################################
f = plt.figure(figsize = (18,12))
plt.title('2D 16 x 16 hexagonal lattice')
plot = f.add_subplot(2,2,1)
plt.plot(t,Energy,'o',color = "#A60628")
plt.xlabel("Temperature (T)", fontsize=20);
plt.ylabel("Energy ", fontsize=20);


plot = f.add_subplot(2,2,2)
plt.plot(t, abs(Magnetization), 'o', color="#348ABD");
plt.xlabel("Temperature (T)", fontsize=20);
plt.ylabel("Magnetization ", fontsize=20);



plot = f.add_subplot(2, 2, 3 );
plt.plot(t, Specificheat, 'o', color="#A60628");
plt.xlabel("Temperature (T)", fontsize=20);
plt.ylabel("Specific Heat ", fontsize=20);



plot = f.add_subplot(2, 2, 4 );
plt.plot(t, Susceptibility, 'o', color="#348ABD");
plt.xlabel("Temperature (T)", fontsize=20);
plt.ylabel("Susceptibility", fontsize=20);

plt.show()


